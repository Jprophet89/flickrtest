//
//  JFImageObject.swift
//  mideraTest
//
//  Created by João Fonseca on 10/02/2018.
//  Copyright © 2018 João Fonseca. All rights reserved.
//

import Foundation
import RealmSwift
import Kingfisher

class JFImageObject : Object {
    //Properties
    @objc dynamic var id : String = ""
    @objc dynamic var title : String = ""
    @objc dynamic var server : String = ""
    @objc dynamic var owener : String = ""
    //Images links
    @objc dynamic var large_square_url : String = ""
    @objc dynamic var large_url : String = ""
  
    //Logic
    override class func primaryKey() -> String? {
        return "id"
    }
    //Large Square Image GET & SET
    func setSquareImage(url : String) -> Void{
        self.large_square_url = url
    }
    func getSquareImage() -> UIImage? {
        let image = ImageCache.default.retrieveImageInDiskCache(forKey: self.large_square_url)
        if(image != nil){
            return image!
        }else{
            return nil
        }
    }
    //Large Image GET & SET
    func setLargeImage(url : String) -> Void{
        self.large_url = url
    }
    func getLargeImage() -> UIImage? {
        let image = ImageCache.default.retrieveImageInDiskCache(forKey: self.large_url)
        if(image != nil){
            return image!
        }else{
            return nil
        }
    }
    //Set information
    func setInformation(information:NSDictionary)->Void{
        self.id = information["id"] as! String
        self.title = information["title"] as! String
        self.server = information["server"] as? String ?? ""
        self.owener = information["owener"] as? String ?? ""
    }
   
    
}
