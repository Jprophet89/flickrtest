//
//  ViewController.swift
//  mideraTest
//
//  Created by João Fonseca on 10/02/2018.
//  Copyright © 2018 João Fonseca. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import Kingfisher

class ListController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    //Bullets
    @IBOutlet weak var collectionView: UICollectionView!
    //Application vars
    
    let realm = try! Realm()
    //missing pagination as a parameter &page=(number)
    let urlToPhotos = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f9cc014fa76b098f9e82f1c288379ea1&tags=kitten&format=json&nojsoncallback=1"
    //missing photoid as a parameter &photo=(number)
    let urlToImages = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=f9cc014fa76b098f9e82f1c288379ea1&format=json&nojsoncallback=1"
    var indexPagination: Int = 1
    var itemsForCollection : [JFImageObject] = []
    private let leftAndRightPaddings: CGFloat = 5.0
    private let numberOfItemsPerRow: CGFloat = 2.0
    
    //Logic
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        //setCell Size
        self.setCellListSize()
        
        //init Collection with pagination 1
        self.getImages(page: self.indexPagination)
        
        //set up infinity scroll
        self.collectionView.addInfiniteScroll { (collectionView) -> Void in
            collectionView.performBatchUpdates({ () -> Void in
                // update collection view
                self.indexPagination = self.indexPagination + 1
                self.getImages(page: self.indexPagination)
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                collectionView.finishInfiniteScroll()
            });
        }
    }
    //set 2 columns with 2 pics for row
    func setCellListSize(){
        //set Collection View layout
        let bounds = UIScreen.main.bounds
        let width = (bounds.size.width - 10 - leftAndRightPaddings*(numberOfItemsPerRow+1)) / numberOfItemsPerRow
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize.width = width
        layout.itemSize.height = width
    }
    
    
    //just to recalibrate the 2 cell per row after rotate the device
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { _ in
            self.setCellListSize()
        })
    }
    
    //callBack to go fullScreen
    func goToFullScreen(item : JFImageObject){
        print(item)
        //get Storyboard
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        //get fullScreen Controller
        let fullScreenController = storyBoard.instantiateViewController(withIdentifier: "fullScreenController") as! FullScreenController
        //Set image for fullScreen
        fullScreenController.imageFromObject = item
        //present
        self.present(fullScreenController, animated:true, completion:nil)        
    }
    //CollectionView Protocol
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsForCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "JFListCell", for: indexPath) as! ListViewCell
        let imageInformation = self.itemsForCollection[indexPath.row]
        //Tap Gesture add to image
        cell.addTapProperty()
        cell.imageView.image = imageInformation.getSquareImage()
        cell.imageObject = imageInformation
        cell.superViewController = self
        return cell;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //Requests
    func getImages(page:Int) -> Void{
        Alamofire.request("\(self.urlToPhotos)&page=\(page)", method:.get).responseJSON { response in
            if let result = response.result.value {
                let JSON = result as! NSDictionary
                let success = JSON["stat"] as? String ?? "notok"
                if(success == "ok"){
                    //get photos object
                    let photos = JSON["photos"] as? NSDictionary ?? nil
                    if(photos != nil){
                        //get photo objetc
                        let allPhotos = photos!["photo"] as? NSArray ?? nil
                        if(allPhotos != nil){
                            //process all photos
                            for item in allPhotos!{
                                //get object
                                let photoObjetc = item as! NSDictionary
                                //exist?
                                print(photoObjetc["id"] as! String)
                                let existingPhoto = self.realm.object(ofType: JFImageObject.self, forPrimaryKey: photoObjetc["id"] as! String)
                                if(existingPhoto == nil){
                                    //if not add to realm
                                    let newPhoto = JFImageObject()
                                    newPhoto.setInformation(information: photoObjetc)
                                    try! self.realm.write {
                                        self.realm.add(newPhoto)
                                    }
                                    self.fetchImages(imageObject: newPhoto)
                                    //add to collection new Photo
                                    self.itemsForCollection.append(newPhoto)
                                }else{
                                    //else add to collection
                                    self.itemsForCollection.append(existingPhoto!)
                                }
                            }
                        }
                    }
                    self.collectionView.reloadData()
                }
            }else{
                //Get Offline from realm and cache
                let listOfImages = self.realm.objects(JFImageObject.self)
                self.itemsForCollection = []
                for item in listOfImages{
                    self.itemsForCollection.append(item)
                }
                self.collectionView.reloadData()
            }
        }
    }
    
    func fetchImages(imageObject: JFImageObject) -> Void{
        Alamofire.request("\(self.urlToImages)&photo_id=\(imageObject.id)", method:.get).responseJSON { response in
            if let result = response.result.value {
                let JSON = result as! NSDictionary
                let success = JSON["stat"] as? String ?? "notok"
                if(success == "ok"){
                    //get sizes object
                    let sizes = JSON["sizes"] as? NSDictionary ?? nil
                    let size = sizes!["size"] as? NSArray ?? nil
                    if(size != nil){
                        //get image size flags
                        var largeFlag = false
                        var largeSquareFlag = false
                        var largeURL = ""
                        var largeSquareURL = ""
                        var originalPhoto = ""
                        var mediumPhoto = ""
                        var smallPhoto = ""
                        for item in size!{
                            let objectSize = item as! NSDictionary
                            //Get other
                            if(objectSize["label"] as! String == "Original"){
                                originalPhoto = objectSize["source"] as! String
                            }
                            if(objectSize["label"] as! String == "Medium"){
                                mediumPhoto = objectSize["source"] as! String
                            }
                            if(objectSize["label"] as! String == "Small"){
                                smallPhoto = objectSize["source"] as! String
                            }
                            //getNeeded Sizes
                            if(objectSize["label"] as! String == "Large"){
                                largeFlag = true
                                largeURL = objectSize["source"] as! String
                                ImageDownloader.default.downloadImage(with: URL(string:largeURL)! , options: [], progressBlock: nil) {
                                    (image, error, url, data) in
                                    if(image != nil){
                                        ImageCache.default.store(image!, forKey: largeURL)
                                    }
                                }
                            }
                            if(objectSize["label"] as! String == "Large Square"){
                                largeSquareFlag = true
                                largeSquareURL = objectSize["source"] as! String
                                ImageDownloader.default.downloadImage(with: URL(string:largeSquareURL)! , options: [], progressBlock: nil) {
                                    (image, error, url, data) in
                                    if(image != nil){
                                        ImageCache.default.store(image!, forKey: largeSquareURL)
                                        for i in 0 ..< self.collectionView.numberOfItems(inSection: 0) {
                                            let cell = self.collectionView.cellForItem(at: IndexPath(row: i, section: 0)) as? ListViewCell
                                            if cell != nil {
                                                if(cell?.imageObject?.id == imageObject.id){
                                                    cell?.imageView?.image = image!
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            //Even if you dont get the original size its not needed because we got the large and large square
                            if(largeFlag && largeSquareFlag){
                                break;
                            }
                        }
                        //save guard only in case of large size dosn't exist
                        if(largeURL.isEmpty){
                            if(!originalPhoto.isEmpty){
                                largeURL = originalPhoto
                            }else{
                                if(!mediumPhoto.isEmpty){
                                    largeURL = mediumPhoto
                                }else{
                                    if(!smallPhoto.isEmpty){
                                        largeURL = smallPhoto
                                    }
                                }
                            }
                            ImageDownloader.default.downloadImage(with: URL(string:largeURL)! , options: [], progressBlock: nil) {
                                (image, error, url, data) in
                                if(image != nil){
                                    ImageCache.default.store(image!, forKey: largeURL)
                                }
                            }
                        }
                        //presist the data on realm
                        try! self.realm.write {
                            imageObject.setLargeImage(url:largeURL)
                            imageObject.setSquareImage(url:largeSquareURL)
                            self.realm.add(imageObject, update: true)
                        }
                    }
                }
            }
        }
    }
    

}

