//
//  FullScreenController.swift
//  mideraTest
//
//  Created by João Fonseca on 10/02/2018.
//  Copyright © 2018 João Fonseca. All rights reserved.
//

import UIKit
import Kingfisher

class FullScreenController: UIViewController , UIScrollViewDelegate {

    @IBOutlet weak var scrollArea: UIScrollView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    var imageFromObject : JFImageObject? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //check if has information
        if(self.imageFromObject == nil){
            print(self.imageFromObject)
            self.goBack()
            return
        }
        //Add Swipe rigt to return to list
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(FullScreenController.goBack))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(FullScreenController.showButton))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapRecognizer)
        // Load Image
        self.image.image = self.imageFromObject?.getLargeImage()
        if(self.image.image == nil){
            ImageDownloader.default.downloadImage(with: URL(string:self.imageFromObject!.large_url)! , options: [], progressBlock: nil) {
                (image, error, url, data) in
                if(image != nil){
                    self.image.image = image
                }
            }
        }
        self.scrollArea.minimumZoomScale = 1.0;
        self.scrollArea.maximumZoomScale = 6.0;
        self.scrollArea.contentSize = self.image.frame.size;
        self.scrollArea.delegate = self;
        self.button.isHidden = true;
    
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.image
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Press Button Back action
    @IBAction func pressButtonBack(_ sender: Any) {
        self.goBack()
    }
    //Function to go back
    @objc func goBack(){
        //get Storyboard
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        //get ListController
        let fullScreenController = storyBoard.instantiateViewController(withIdentifier: "listController") as! ListController
        //present
        self.present(fullScreenController, animated:true, completion:nil)
    }
    
    //animation for the back button and Timeout
    func setTimeout(_ delay:TimeInterval, block:@escaping ()->Void) -> Timer {
        return Timer.scheduledTimer(timeInterval: delay, target: BlockOperation(block: block), selector: #selector(Operation.main), userInfo: nil, repeats: false)
    }
    @objc func showButton(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.button.isHidden = false
            self.view.layoutIfNeeded()
            self.setTimeout(3, block: { () -> Void in
                self.hideButton()
            })
        })
    }
    func hideButton(){
        UIView.animate(withDuration: 0.3, animations: {
            self.button.isHidden = true
            self.view.layoutIfNeeded()
        })
    }
}
