//
//  ListViewCell.swift
//  mideraTest
//
//  Created by João Fonseca on 10/02/2018.
//  Copyright © 2018 João Fonseca. All rights reserved.
//

import UIKit

class ListViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var imageObject : JFImageObject? = nil
    var superViewController : UIViewController? = nil
    
    func addTapProperty(){
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ListViewCell.tapRecognition))
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func tapRecognition(){
        let controller = self.superViewController as! ListController
        controller.goToFullScreen(item: self.imageObject!)
    }
}
